"""Cryptopals S1C1 - Convert hex to base64"""
#libraries for working with decoding HEX to binary and binary to base64
import binascii
import base64

#Enter a hex string that will convert into base64
#Cryptopals example: 49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d
hex_string = input("Enter original string: ")

#For validation purposes, define what we know has to be solution. Encode() here is necesarry to get that string into same charset (UTF-8) as the result of b64encode() will be
final_result = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t".encode()

#Create a function for this operation
def hex2b64(s): #the "s" in parenthesis is a placeholder for anything that will come when this function is called
  hex_binary = binascii.unhexlify(s) #to decode hex input into binary, we will use unhexlify fuction within binascii library...
  base64_string = base64.b64encode(hex_binary) #...and then encode that binary string into base64, using b64encode function within base64 library
  print("Base64 string:\n", base64_string) #Print out the base64 string
  assert base64_string == final_result, "Not the same" #check if our result is the same as the result we were supposed to get

#Call the function with the hex_string parameter 
hex2b64(hex_string)