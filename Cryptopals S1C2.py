"""Fixed XOR"""
#Write a function that takes two equal-length buffers and produces their XOR combination.
#If your function works properly, then when you feed it the string:
#1c0111001f010100061a024b53535009181c
#... after hex decoding, and when XOR'd against:
#686974207468652062756c6c277320657965
#... should produce:
#746865206b696420646f6e277420706c6179
import binascii
hex1 = input("Enter 1st hex string: ")
hex2 = input("Enter 2nd hex string: ")

def fixed_xor(s,n):
  c = "746865206b696420646f6e277420706c6179".encode()

  #decoding hex into binary
  #a_bin = binascii.unhexlify(s)
  #b_bin = binascii.unhexlify(n)
  a_bin = int(hex1, 16)
  b_bin = int(hex2, 16)
  
  result = a_bin^b_bin

  print ("final: ",result)
  #hex_binary = bin(int(hex_string, 16))


fixed_xor(hex1,hex2)